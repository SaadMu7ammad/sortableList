## Sortable List

Display a scrambled list that can be sorted with drag and drop

## Project Specifications

- Create an ordered list (Top 10 richest people)
- Scramble list items randomly
- Allow user to drag and drop an item to a different position
- Button to check if items are in correct order
- Show green for correct order and red for wrong order

[check it out](https://sortablelist-saadmu7ammad-33d4aceba6ece6421445de12275239af6d65a.gitlab.io/)